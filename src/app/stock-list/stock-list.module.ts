import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
} from '@angular/material';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsModule } from '@ngxs/store';

import { DataTableComponent } from './data-table/data-table.component';
import { FilterComponent } from './filter/filter.component';
import { StockListRoutingModule } from './stock-list-routing.module';
import { StockListComponent } from './stock-list.component';
import { states } from './store';

@NgModule({
  declarations: [StockListComponent, DataTableComponent, FilterComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatTableModule,
    NgxsModule.forRoot(states),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: false,
    }),
    ReactiveFormsModule,
    StockListRoutingModule,
  ],
  exports: [StockListComponent],
})
export class StockListModule {}
