import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { StockQuote } from '../models/stock-quote';
import { UpdateLoadingIndicator } from '../store/loading-indicator/loading-indicator.action';

@Injectable({
  providedIn: 'root',
})
export class StockMarketService {
  private readonly _baseUrl = 'https://api.iextrading.com/1.0';
  constructor(private readonly _http: HttpClient, private readonly _store: Store) {}

  public getHealthCareStocks$(): Observable<Array<StockQuote>> {
    this._store.dispatch(new UpdateLoadingIndicator(true));

    return this._http.get<Array<StockQuote>>(
      `${this._baseUrl}/stock/market/collection/sector?collectionName=Health%20Care`,
    );
  }
}
