import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { StockQuote } from './models/stock-quote';
import { FetchStocks } from './store/stock/stock.actions';
import { StockState } from './store/stock/stock.state';

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
})
export class StockListComponent implements OnInit {
  public dataSource = new MatTableDataSource<StockQuote>();
  public displayedColumns: string[] = ['companyName', 'symbol', 'latestPrice', 'change', 'primaryExchange'];

  @Select(StockState.stocks) private _stocks$: Observable<StockQuote[]>;
  @Select((state) => state.loadingIndicator.isLoading) public isLoading$: Observable<boolean>;

  constructor(private readonly _store: Store) {
    this._store.dispatch(new FetchStocks());
  }

  public ngOnInit() {
    this._stocks$.subscribe((stocks) => {
      this.dataSource.data = stocks;
    });
  }
}
