export class SetMinimumPrice {
  public static readonly type = '[FILTER] set minimum price';

  constructor(public payload: number) {}
}

export class SetMaximumPrice {
  public static readonly type = '[FILTER] set maximum price';

  constructor(public payload: number) {}
}
