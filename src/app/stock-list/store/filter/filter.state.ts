import { Action, State, StateContext } from '@ngxs/store';

import { SetMaximumPrice, SetMinimumPrice } from './filter.actions';

export class FilterStateModel {
  public minimumPrice?: number;
  public maximumPrice?: number;
}

@State<FilterStateModel>({
  name: 'filters',
  defaults: {
    minimumPrice: null,
    maximumPrice: null,
  },
})
export class FilterState {
  @Action(SetMinimumPrice)
  public setMinimumPrice(context: StateContext<FilterStateModel>, action: SetMinimumPrice) {
    context.patchState({ minimumPrice: action.payload });
  }

  @Action(SetMaximumPrice)
  public setMaximumPrice(context: StateContext<FilterStateModel>, action: SetMaximumPrice) {
    context.patchState({ maximumPrice: action.payload });
  }
}
