import { FilterState } from './filter/filter.state';
import { LoadingIndicatorState } from './loading-indicator/loading-indicator.state';
import { StockState } from './stock/stock.state';

export const states = [StockState, FilterState, LoadingIndicatorState];
