import { Action, Selector, State, StateContext } from '@ngxs/store';
import { delay, mergeMap, tap } from 'rxjs/operators';

import { StockQuote } from '../../models/stock-quote';
import { StockMarketService } from '../../services/stock-market.service';
import { FilterState, FilterStateModel } from '../filter/filter.state';
import { UpdateLoadingIndicator } from '../loading-indicator/loading-indicator.action';

import { FetchStocks } from './stock.actions';

@State<StockQuote[]>({
  name: 'stocks',
  defaults: [],
})
export class StockState {
  constructor(private readonly _stockMarketService: StockMarketService) {}

  @Selector([FilterState])
  public static stocks(state: StockQuote[], filterState: FilterStateModel) {
    return state.filter(
      (stock) =>
        (filterState.minimumPrice === null || stock.latestPrice > filterState.minimumPrice) &&
        (filterState.maximumPrice === null || stock.latestPrice < filterState.maximumPrice),
    );
  }

  @Action(FetchStocks)
  public fetch(context: StateContext<StockQuote[]>) {
    return this._stockMarketService.getHealthCareStocks$().pipe(
      delay(2000),
      tap((stocks) => {
        context.setState(stocks);
      }),
      mergeMap(() => context.dispatch(new UpdateLoadingIndicator(false))),
    );
  }
}
