import { Action, State, StateContext } from '@ngxs/store';

import { UpdateLoadingIndicator } from './loading-indicator.action';

export class LoadingIndicatorStateModel {
  public isLoading: boolean;
}

@State<LoadingIndicatorStateModel>({
  name: 'loadingIndicator',
  defaults: { isLoading: false },
})
export class LoadingIndicatorState {
  @Action(UpdateLoadingIndicator)
  public update(context: StateContext<LoadingIndicatorStateModel>, action: UpdateLoadingIndicator) {
    context.setState({ isLoading: action.isLoading });
  }
}
