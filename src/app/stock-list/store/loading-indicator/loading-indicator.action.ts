export class UpdateLoadingIndicator {
  public static readonly type = '[LOADING-INDICATOR] update';

  constructor(public isLoading: boolean) {}
}
