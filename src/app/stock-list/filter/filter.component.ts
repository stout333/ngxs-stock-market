import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { combineLatest, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';

import { SetMaximumPrice, SetMinimumPrice } from '../store/filter/filter.actions';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterComponent implements OnInit, OnDestroy {
  public filters: FormGroup;

  private readonly _onDestroy$ = new Subject();

  constructor(private readonly _formBuilder: FormBuilder, private readonly _store: Store) {
    this.filters = this._formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
      minimumPrice: ['', Validators.pattern(/^\d*$/)],
      maximumPrice: ['', Validators.pattern(/^\d*$/)],
    });
  }

  public ngOnInit() {
    this.getValidValues('minimumPrice').subscribe(([value]) => {
      this._store.dispatch(new SetMinimumPrice(value));
    });

    this.getValidValues('maximumPrice').subscribe(([value]) => {
      this._store.dispatch(new SetMaximumPrice(value));
    });
  }

  public ngOnDestroy(): void {
    this._onDestroy$.next();
  }

  private getValidValues(formControlName: string): Observable<any> {
    return combineLatest(
      this.filters.controls[formControlName].valueChanges,
      this.filters.controls[formControlName].statusChanges,
    ).pipe(
      takeUntil(this._onDestroy$),
      debounceTime(400),
      distinctUntilChanged(),
      filter(([, status]) => status === 'VALID'),
    );
  }
}
